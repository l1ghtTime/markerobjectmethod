const markerArea = document.querySelector('.marker__area'),
      markerBtnFullInk = document.querySelector('.marker__full'),
      markerBtnItemInk = document.querySelector('.marker__step'),
      wrightArea = document.querySelector('.wright-area'),
      bgItems = Array.from(document.querySelectorAll('.bg__item')),
      score = document.querySelector('.score__text');

const regular = {
    reg: /\s/g,
    regAfter: /[a-z]\s+[a-z]/ig,
};

const markerState = {
    bgState: '',
};

const createItem = () => {
    
    if(markerArea.children.length > 9) {
        return false;
    }

    const item = document.createElement('div');
    item.classList.add('marker-item');
    markerArea.appendChild(item);

    item.style.backgroundColor = `${markerState.bgState}`;    
};

const marker = {

    observe: [],
    counter: 0,

    addObserve(fn) {
        this.observe.push(fn);
    },

    trigerItem(){
        this.observe.forEach(item => item());
    },

    moveStep(value) {
        this.counter = value;
        for(let i = 1; i <= this.counter; i++) {                              
            this.trigerItem();
        }
    },

    removeStep() {
        let arrParent = Array.from(markerArea.children)
            lastElem = arrParent[arrParent.length - 1];

        markerArea.removeChild(lastElem);
        arrParent.pop();

        if(arrParent.length === 0) {
            wrightArea.setAttribute('disabled', '');

            setTimeout(() => {
                alert('У вас закончились чернила, пополните их запас!');
            }, 500);
        }
    },
};

marker.addObserve(createItem);

marker.moveStep(10);

const inputArea = {

    outgo(value) {
        if(value % 5 === 0 && regular.reg.test(wrightArea.value) === false) {
            marker.removeStep();
        }
    },

};

const markerItems = Array.from(document.querySelectorAll('.marker-item'))

wrightArea.addEventListener('input', () => {

    inputArea.outgo(wrightArea.value.length);
    score.innerHTML = wrightArea.value.length;

    let newString = wrightArea.value.replace(regular.regAfter, '');
    wrightArea.value = newString;
});

markerBtnFullInk.addEventListener('click', () => {
    marker.moveStep(10);
    wrightArea.removeAttribute('disabled');
});

markerBtnItemInk.addEventListener('click', () => {
    marker.moveStep(1);
    wrightArea.removeAttribute('disabled');
});


bgItems.forEach(item => {
    item.addEventListener('click', () => {
       let elem = event.target;

       markerState.bgState = window.getComputedStyle(elem).backgroundColor;

       markerItems.forEach(item => {
            item.style.backgroundColor = `${markerState.bgState}`;
       });

       wrightArea.style.color = `${markerState.bgState}`;
    });
});



